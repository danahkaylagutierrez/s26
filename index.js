const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'content-type': 'text/plain'})
		response.end('You are in the log in page.')
		}

	else{
		response.writeHead(404, {'content-type': 'text/plain'})
		response.end('Error. Page is not available')
	}
})
server.listen(port);

console.log(`Server now accessible at local host:${port}`);